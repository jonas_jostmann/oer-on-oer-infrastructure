<!--- Local IspellDict: en -->

# Introduction

This project hosts
[Open Educational Resources (OER)](https://en.wikipedia.org/wiki/Open_educational_resources)
explaining OER infrastructures based on
[free/libre and open source software (FLOSS)](https://en.wikipedia.org/wiki/Free_and_open-source_software).

If you want to learn or teach what OER are, you should be able to find
suitable OER courses (like
[this course (in German) at the university of Göttingen, Germany](https://openilias.uni-goettingen.de/openilias/goto_openilias_crs_369.html)).

If you are creating OER yourself and would like to document your
infrastructure here, feel free to contact me.  So far, only various
aspects of my own infrastructure surrounding
[emacs-reveal](https://gitlab.com/oer/emacs-reveal)
are explained here.

# Emacs-reveal

I use [emacs-reveal](https://gitlab.com/oer/emacs-reveal) to
create OER presentations (slides with audio) based on the HTML
presentation framework
[reveal.js](https://github.com/hakimel/reveal.js/).  I created my own
infrastructure because some features were missing in all alternatives
that I analyzed.  With emacs-reveal, students can work through
presentations on essentially *any device* (with a recent Web browser,
preferably [Firefox](https://www.mozilla.org/en-US/firefox/)), even if
they are *offline*.

With emacs-reveal, instructors compose OER presentations in a simple
text format called [Org Mode](https://orgmode.org/) using the text editor
[GNU Emacs](https://www.gnu.org/software/emacs/).
Simple text files offer two major benefits:
1. Layout and style of presentations are separated from content to
   simplify *collaboration across organizational boundaries*.
2. To maintain a [single source](http://www.rockley.com/articles/Single_Sourcing_and_Technology.pdf)
   when collaborating on *versions* of presentations, proven version
   control systems such as Git are applicable, which enable comparison
   and merging; e.g. [compare functionality on GitLab](https://gitlab.com/oer/OS/compare/os02...os03).

OER presentations related to emacs-reveal:
- [Emacs-reveal howto](https://oer.gitlab.io/emacs-reveal-howto/howto.html)
- [Git introduction](https://oer.gitlab.io/oer-on-oer-infrastructure/Git-introduction.html)
