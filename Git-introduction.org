# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: config.org
#+OPTIONS: toc:1

#+TITLE: Git Introduction
#+DATE: Summer Term 2018
#+AUTHOR: Jens Lechtenbörger
#+REVEAL_ACADEMIC_TITLE: Dr.

* Introduction
** Learning Objectives
   - Explain benefits of [[#vcs][version control systems]] (e.g., in the context
     of university study) and [[#vcs-review][contrast]] distributed ones with
     centralized ones
   - Explain [[#states-review][states of files under Git]] and apply commands to manage them
   - Explain [[#git-workflow][Feature Branch Workflow]] and apply it
     in sample scenarios
   - Edit simple [[#markdown][Markdown]] documents

** Core Questions
   - How to *collaborate* on shared documents as distributed team?
     {{{reveallicense("./figures/screenshots/magit.meta","45vh")}}}
     - Consider *multiple* people working on *multiple* files
       - Potentially in *parallel* on the *same* file
       - Think of group exercise sheet, project documentation, source
         code
   - How to keep track of who changed what why?
   - How to support unified/integrated end result?

** Version Control Systems (VCSs)
   :PROPERTIES:
   :CUSTOM_ID: vcs
   :END:
   - Synonyms: Version/source code/revision control system, source
     code management (SCM)
   - Collaboration on *repository* of documents
     - Each document going through various versions/revisions
       - Each document improved by various authors
	 - April 2012, Linux kernel 3.2: [[https://www.linux.com/learn/counting-contributions-who-wrote-linux-32][1,316 developers from 226 companies]]

*** Major VCS features
    #+ATTR_REVEAL: :frag (appear)
    - VCS keeps track of *history*
      - Who changed what/why when?
      {{{reveallicense("./figures/abstract/arrows-2033963_1920.meta","35vh")}}}
    - VCS supports *merging* of versions into *unified/integrated*
      version
      - Integrate intermediate versions of single file with changes by
  	multiple authors
    - Copying of files is obsolete with VCSs
      - Do *not* create copies of files with names such as
	~Git-Intro-Final-1.1.txt~ or
	~Git-Intro-Final-reviewed-Alice.txt~
	- Instead, use VCS mechanism, e.g., use
          [[https://git-scm.com/book/en/v2/Git-Basics-Tagging][tags]] with Git

* Git Concepts

** Git: A Distributed VCS
   - Various VCSs exist
     - E.g.: [[color:darkgreen][Git]], [[color:darkgreen][BitKeeper]],
       [[color:darkred][svn]], [[color:darkred][cvs]]
       - (Color code: [[color:darkgreen][distributed]],
         [[color:darkred][centralized]])
   - Git created by Linus Torvalds for the development of the [[https://www.kernel.org/][kernel Linux]]
     - Reference: [[https://git-scm.com/book/en/v2][Pro Git book]]
       {{{reveallicense("./figures/logos/Git-Logo-2Color.meta","15vh")}}}
     - Git as example of *distributed* VCS
       - Every author has *own copy* of all documents and their history
       - Supports *offline* work without server connectivity
	 - Of course, collaboration requires network connectivity

** Key Terms: Fork, Commit, Push, Pull
   - *Fork/clone* repository
     {{{reveallicense("./figures/icons/cloned-folder.meta","8vh")}}}
     - Create your own copy of a repository
   #+ATTR_REVEAL: :frag appear
   - *Commit* (aka check-in)
     {{{reveallicense("./figures/icons/changed-folder.meta","18vh")}}}
     - Make (some or all) changes permanent; announce them to version
       control system
     - *Push*: Publish (some or all) commits to remote location
     - *Fetch* (*pull*): Retrieve commits from remote location (also
       merge them)

** Key Terms: Branch, Merge
   :PROPERTIES:
   :CUSTOM_ID: git-branch
   :END:
   - *Branches*
     {{{reveallicense("./figures/git/git-branches.meta","30vh")}}}
     - Alternative versions of documents, on which to commit
       - Without being disturbed by changes of others
       - Without disturbing others
	 - You can share your branches if you like, though
   - *Merge*
     - Combine changes of one branch into another branch
       - May or may not need to resolve conflicts

** Git explained by Linus Torvalds
   @@html:<div class="imgcontainer"><video controls width="400px" height="300px" src="https://archive.org/download/LinusTorvaldsOnGittechTalk/LinusTorvaldsOnGittechTalk.ogv#t=460"></video></div>@@

   - [[https://archive.org/details/LinusTorvaldsOnGittechTalk][Video at archive.org]]
     (Tech Talk, 2007, by Google Talks under
     [[http://creativecommons.org/licenses/by-nc-sa/3.0/][CC BY-NC-SA 3.0]])
     - Total length of 84 minutes, suggested viewing: 7:40 to 29:00

*** Review Questions
   :PROPERTIES:
   :CUSTOM_ID: vcs-review
   :END:
    Answer the following questions in [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=1006314][Learnweb]].
    - What is the role of a VCS (or SCM, in Torvald’s terminology)?
    - What differences exist between distributed and centralized VCSs?


* Git Basics

** First Steps
   :PROPERTIES:
   :CUSTOM_ID: git-demo
   :END:
   - Start with [[https://try.github.io/levels/1/challenges/1][in-browser demo]]

*** Review Questions
   :PROPERTIES:
   :CUSTOM_ID: states-review
   :END:
    - As part of [[#git-demo][in-browser demo]], ~git status~
      inspects repository, in particular file *states*
      - Recall that files may be ~untracked~, if they are located
        inside a Git repository but not managed by Git
      - Other files may be called ~tracked~
    - Answer the following questions in [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=1006314][Learnweb]].
      Among the ~tracked~ files, which states can you identify from
      the demo?  Which commands are presented to perform what state
      transitions?
      - Optional: Draw a diagram to visualize your findings

** Getting Started
   - [[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git][Install Git]]
   - You may use Git without a server
      - Run ~git init~ in any directory (as in [[#git-demo][in-browser demo above]])
      - Keep track of your own files
      - By default, you work on the ~master~ branch

** Git with Remote Repositories
   - *Download* files from public repository: ~clone~
     - ~git clone https://gitlab.com/oer/oer-on-oer-infrastructure.git~
       - Later on, ~git pull~ merges changes to bring your copy up to date
   - *Contribute* to remote repository
     - Create account first
       - Typically, ~ssh~ key pairs ([[#ssh][next slide]]) are used for
	 strong authentication; register under your account’s settings
     - Fork project
       - either in GUI
       - or clone your copy, [[https://www.atlassian.com/git/articles/git-forks-and-upstreams][add upstream]]

*** Secure Shell
    :PROPERTIES:
    :CUSTOM_ID: ssh
    :END:
    - [[https://en.wikipedia.org/wiki/Secure_Shell][Secure Shell]]
      (~ssh~): network protocol for remote login with end-to-end
      encryption based on
      [[file:../OS/OS10-Security.org::#asym-intuition][asymmetric cryptography]]
      - Popular [[https://en.wikipedia.org/wiki/Free_and_open-source_software][free]] implementation: [[https://www.openssh.com/][OpenSSH]]
	- Tool to create key pair: ~ssh-keygen~
    - [[https://docs.gitlab.com/ce/ssh/README.html][Instructions on GitLab]]
      - (In case you are affected, note that [[https://gitforwindows.org/][Git Bash on Windows]]
        is mentioned)

** Merge vs Rebase
   - Merge and rebase unify two [[#git-branch][branches]]
     - (Recall ~merge~ of branch ~clean_up~ in [[#git-demo][in-browser demo above]])
   - Illustrated subsequently
     - Same unified result

*** Merge vs Rebase (1)
    - Suppose you created branch for new ~feature~ and committed on that
      branch; in the meantime, somebody else committed to ~master~

    {{{reveallicense("./figures/git/forked-commit-history.meta","40vh")}}}

*** Merge vs Rebase (2)
    - Merge creates *new* commit to combine both branches
      - Including all commits
      - Keeping parallel history

    {{{reveallicense("./figures/git/merged-feature.meta","40vh")}}}

*** Merge vs Rebase (3)
    - Rebase rewrites ~feature~ branch on ~master~
      - Applies commits of ~feature~ on ~master~
      - Cleaner end result, but branch’s history lost/changed

    {{{reveallicense("./figures/git/rebased-feature.meta","40vh")}}}

** Git Workflows
    :PROPERTIES:
    :CUSTOM_ID: git-workflow
    :END:
   - Team needs to agree on *git workflow*
     - [[https://www.atlassian.com/git/tutorials/comparing-workflows][Several alternatives]] exist
   - [[https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow][Feature Branch Workflow]]
     may be your starting point
     - Fork remote repository
     - Create separate branch for *each* independent contribution
       - E.g., bug fix, new feature, improved documentation
       - Enables independent work
     - Once done, push that branch, create pull request, receive feedback
       - *Pull request*: special action asking maintainer to include
         your changes
       - Maintainer may merge branch into master

* GitLab

** GitLab Overview
   - Web platform for Git repositories
     - [[https://about.gitlab.com/]]
     - Free software, which you could run on your own server
   - Manage Git repositories
     - Web GUI for forks, commits, pull requests, issues, and much more
     - Notifications for lots of events
       - Not enabled by default
     - So-called Continuous Integration (CI) runners to be executed upon
       commit
       - Based on Docker images
       - Build whatever needs building in your project (executables,
	 documentation, presentations, etc.)

** GitLab in Action
   - In class

* Aside: Lightweight Markup Languages
** Lightweight Markup
   - Markup: “Tags” for annotation in text, e.g., indicate sections and
     headings, emphasis, quotations, …
   - [[https://en.wikipedia.org/wiki/Lightweight_markup_language][Lightweight markup]]
     - ASCII-only punctuation marks for “tags”
     - Human readable, simple syntax, standard text editor sufficient
       to read/write
     - Tool support
       - Comparison and merge, e.g.,
         [[https://en.wikipedia.org/wiki/Merge_(version_control)#Three-way_merge][three-way merge]]
       - Conversion to target language (e.g. (X)HTML, PDF, EPUB, ODF)
	 - Wikis, blogs
	 - [[http://pandoc.org/][pandoc]] can convert between
	   lots of languages

** Markdown
   :PROPERTIES:
   :CUSTOM_ID: markdown
   :END:
   - [[https://en.wikipedia.org/wiki/Markdown][Markdown]]: A lightweight markup language
   - Every Git repository should include a README file
     - What is the project about?
     - Typically, ~README.md~ in Markdown syntax
   - Learning Markdown
     - [[https://www.markdowntutorial.com][In-browser tutorial]]
       (source code under [[https://github.com/gjtorikian/markdowntutorial.com/blob/master/LICENSE.txt][MIT License]])
     - [[https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet][Cheatsheet]]
       (under CC BY 3.0)

** Org Mode
   - [[https://orgmode.org/][Org mode]]: Another lightweight markup language
     - My favorite one
   - Details later on, see [[https://gitlab.com/oer/oer-on-oer-infrastructure/blob/master/Git-introduction.org][source file for this presentation as example]]

* Conclusions
** Summary
   - VCSs enable collaboration on files
     - Source code, documentation, theses, presentations
   - Distributed VCSs such as Git enable distributed, in particular
     offline, work
     - Keeping track of files’ states
       - With support for subsequent merge of divergent versions
     - Workflows may prescribe use of branches for pull requests
   - Documents with lightweight markup are particularly well-suited
     for Git management

** Concluding Questions
   - Submit your answer to the following question in [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=1006314][Learnweb]].
   - What did you find difficult or confusing about the *contents* of
     the presentation?  Please be as specific as possible.  For
     example, you could describe your current understanding (which
     might allow us to identify misunderstandings), ask questions that
     allow us to help you, or suggest improvements (ideally by
     creating an issue or pull request in
     [[https://gitlab.com/oer/oer-on-oer-infrastructure][GitLab]]).

#+MACRO: copyrightyears 2018
#+INCLUDE: license-template.org
